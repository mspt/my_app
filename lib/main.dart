
import 'package:flutter/material.dart';

main(){
  runApp(my_app());
}

class my_app extends StatelessWidget {
  const my_app({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
        body: Center(
          child: Stack(
            children: <Widget>[
          Container(
            alignment: Alignment.topCenter,
              child: Image.asset('assets/images/1.jpg'),
              ),

              Column(children: [
                  Container(
                      alignment: Alignment.topLeft,
                      margin: const EdgeInsets.only(top: 102.5,left: 27, right: 0,bottom: 0 ),
                      child: Text('WE ARE \nWHAT WE EAT',
                        style: TextStyle(color: Colors.black,
                            fontWeight: FontWeight.bold,
                            fontSize: 40.0,
                            fontFamily: 'Poppins',
                            height: 0),
                      )
                  ),
                Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(top: 10,left: 27, right: 100,bottom: 0  ),
                    child: Text('Delicious, nutritious, sustained, \nand budget-friendly',
                      style: TextStyle(color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 13.0,
                          fontFamily: 'Poppins' ),
                    )
                ),
                Container(
                    alignment: Alignment.topLeft,
                    margin: const EdgeInsets.only(top: 190,left: 27, right: 100,bottom: 0  ),
                    child: Text('Login',
                      style: TextStyle(color: Colors.black,
                          //fontWeight: FontWeight.bold,
                          fontSize: 21.0,
                          fontFamily: 'Poppins' ),
                    )
                ),
              ],)
            ],
          ),
        ),
      )
    );
  }
}
